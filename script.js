//mini activity
db.products.insertMany([
		{
			name: "Xperia Pro I",
			price: 60000,
			isActive: true
		},
		{
			name: "Iphone 13",
			price: 55000,
			isActive: true
		},
		{
			name: "Xperia 1",
			price: 30000,
			isActive: false
		},
		{
			name: "Playstation 2",
			price: 15000,
			isActive: false
		},
		{
			name: "Predator",
			price: 60000,
			isActive: true
		}
	])


//query operators
//$gt - greather than
db.products.find(
		{
			price: {$gt:30000}
		}
	)

db.products.find(
		{
			price: {$gt: 15000}
		}
	)

// $lt - less than
db.products.find(
		{
			price: {$lt: 50000}
		}
	)

db.products.find(
		{
			price: {$lt: 20000}
		}
	)

//$gte - greater than or equal

db.products.find(
		{
			price: {$gte: 30000}
		}
	)

db.products.find(
		{
			price: {$gte: 50000}
		}
	)

//$lte - less than or equal

db.products.find(
		{
			price: {$lte: 50000}
		}
	)

db.products.find(
		{
			price: {$lte: 60000}
		}
	)

//query ops on update and delete
db.products.updateMany(
		{
			price: {$gte: 50000}
		},
		{
			$set: {
				isActive: true
			}
		}
	)

db.users.insertMany([
		{
			firstName: "Mary Jane",
			lastName: "Watson",
			email: "mjtiger@gmail.com",
			password: "tigerjackpot15",
			isAdmin: false
		},
		{
			firstName: "Gwen",
			lastName: `Stacy`,
			email: `stacyTech@gmail.com`,
			password: `stacyTech1991`,
			isAdmin: true
		},
		{
			firstName: `Peter`,
			lastName: `Parker`,
			email: `peterWebDev@gmail.com`,
			password: `webdeveloperPeter`,
			isAdmin: true
		},
		{
			firstName: `Jonah`,
			lastName: `Jameson`,
			email: `jjjameson@gmail.com`,
			password: `spideyisamenace`,
			isAdmin: false
		},
		{
			firstName: `Otto`,
			lastName: `Octavius`,
			email: `ottoOctopi@gmail.com`,
			password: `docOck15`,
			isAdmin: true
		},
	])

//$regex - oprtr that will allow to match documents which will match the pattern/chars that we are looking for

db.users.find(
		{
			firstName: {$regex: `O`}
		}
	)

db.users.find(
		{
			firstName: {$regex: `o`}
		}
	)

//$options - used to make $regex non-case sensitive
db.users.find(
		{
			firstName: {$regex: `O`, $options: `$i`}
		}
	)

//$regex to find documents which matches a spcecific pattern or word in a field
db.users.find(
		{
			email: {$regex: `web`, $options: `i`}
		}
	)

db.products.find(
		{
			name: {$regex: `phone`, $options: `$i`}
		}
	)

//mini act - using regex, find fr products which has razer and rakk in it

db.products.insertMany([
		{
			name: `RAKK Gaming Mouse`,
			price: 18000,
			isActive: true
		},
		{
			name: `Razer smartphone`,
			price: 25000,
			isActive: true
		}
	])

db.products.find(
			{
				name: {$regex: `xperia`, $options: `$i`}
			}
		)

db.products.find(
			{
				name: {$regex: `rakk`, $options: `$i`}
			}
		)

db.products.updateOne(
		{
			name: {$regex: `Razer smartphone`, $options: `$i`}
		},
		{
			$set: {
				name: `Razerr Blackshark V2X`
			}
		}
	)

//$or, $and
db.products.find(
		{
			$or: [{name: {$regex: `X`, $options: `$i`}}, {price: {$lte: 30000}}]
		}
	)

db.products.find(
		{
			$and: [{name: {$regex: `X`, $options: `$i`}}, {price: {$lte: 30000}}]
		}
	)

db.users.find(
		{
			$or: [{firstName: {$regex: `a`, $options: `$i`}}, {isAdmin: true}]
		}
	)

db.products.find(
		{
			$and: [{name: {$regex: `razer`, $options: `$i`}}, {price: {$lte: 30000}}]
		}
	)

db.users.find(
		{
			$and: [
				{firstName: {$regex: `e`, $options: `$i`}},
				{lastName: {$regex: `a`, $options: `$i`}}
			]
		}
	)

db.products.find(
		{
			$or:
			[
				{price: {$gt: 30000}}, 
				{price: {$lt: 55000}}
			]
		}
	)

//field projection - inclusion or exclusion of fields in the returned documents
//find({query}, {projection})

db.users.find({}, {_id: 0, password: 0}) //hides specific fields
db.users.find({}, {_id: 0, password: 1})

//mini act - look for users who are admins and show only their emails

db.users.find(
		{isAdmin: true},
		{_id: 0, email: 1}
	)

//mini act - find products with letter 'y' in the name AND price
//less than 10000, show only name and price

//find products with letter 'mouse' in the name, show only its name and price

db.products.find(
		{
			$and:
			[
				{name: {$regex: `y`, $options: `$i`}},
				{price: {$lt: 10000}}
			]
		},
		{
			_id: 0,
			name: 1,
			price: 1
		}
	)

db.products.find(
		{
			name: {$regex: `mouse`, $options: `$i`}
		},
		{
			_id: 0,
			name: 1,
			price: 1
		}
	)
